package com.example.autoattendance;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;

public class StudentActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student);
		//find name in database

		View textView1 = this.findViewById(R.id.textView1); 
		((TextView) textView1).setText(getName(getIntent().getExtras().getString("username"))); 

		View textView2=this.findViewById(R.id.textView2); 
		((TextView) textView2).setText(getIntent().getExtras().getString("username")); 
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	public String getName(String s){
		return  "name";
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_message, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_student,
					container, false);
			return rootView;
		}
	}
	public void schedule(View view) {
		Intent intent = new Intent(this, ScheduleActivity.class);
		intent.putExtra("username",getIntent().getExtras().getString("username"));
		startActivity(intent);
	}
	public void attendance(View view) {
		Intent intent = new Intent(this, AttendanceActivity.class);
		intent.putExtra("username",getIntent().getExtras().getString("username"));
		startActivity(intent);
	}
	public void courseList(View view) {
		Intent intent = new Intent(this, CourseListActivity.class);
		intent.putExtra("username",getIntent().getExtras().getString("username"));
		startActivity(intent);
	}
	public void settings(View view) {
		Intent intent = new Intent(this, SettingsActivity.class);
		intent.putExtra("username",getIntent().getExtras().getString("username"));
		startActivity(intent);
	}



}
