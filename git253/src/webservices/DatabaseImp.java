package webservices;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


@WebService(endpointInterface = "webservices.Database", portName = "DatabasePort", serviceName = "Database")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class DatabaseImp implements Database {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String MYSQL_DB_HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
	static final String MYSQL_DB_PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
	static final String DB_URL = "jdbc:mysql://" + MYSQL_DB_HOST + ":" + MYSQL_DB_PORT + "/253attendance";

	// Database credentials
	static final String USER = "adminaNWvx2J"; // TO BE REPLACED
	static final String PASS = "SNFuMd3N9ttN"; // TO BE REPLACED

	@Override
	public String write(int id, String email, String name) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
            // insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "INSERT INTO Student VALUES (" +"'"+ id +"'" + "," + "'"+email +"'" +","+ "'"+name+"'" +")";
			stmt.executeUpdate(sql);
			return "Inserted records into the table...";

		} catch (Exception e) {
			return e.getMessage();
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				return e.getMessage();
			}
		}
	}
	@Override
	public String read(String email) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
            // insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT id,email,name FROM Student WHERE email = "+ "'" +email+"'" ;
			ResultSet rs = stmt.executeQuery(sql);
			String result="";
			while(rs.next())
			{
				int id = rs.getInt(1);
				String emailDB = rs.getString(2);
				String name = rs.getString(3);
				result+= id+ " " +emailDB +" "+name+ "\n";
			}
			return result;

		} catch (Exception e) {
			return e.getMessage();
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				return e.getMessage();
			}
		}
	}
	

}
