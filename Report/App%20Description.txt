253 Project
Automated-Attendace Taker
	Our app is basically an automated attendance taker. It will be a downloadable app that both students and teachers can 
	download. The app will have a login page to differentiate between the student and the teacher. By logging in, the app will
	load the schedule of the student which the student can view any time they wish. The application will then record the GPS 
	location of the student when a class begins, and will continue to record their location in 20 minute intervals and once more
	at the end of class. This will avoid a student attempting to fake presence in the class by standing nearby and then leaving.
	This location will be sent to a database and processed to conclude whether the student was in fact present or not.
	The information will ofcourse be encrypted since it is sensitive and private data. At the end of class the database will
	produce a list of all students who attended the class and who were not present. This list can be accessed by the teachers
	of the course by logging into the application with their credentials. This app eliminates the need for the students or 
	teachers to do anything besides the teacher checking the list.