package webservices;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Database {
	@WebMethod String addStudent(int id, String email, String name, int c1, int c2, int c3, int c4, int c5, String password);
	@WebMethod String getStudent(String email);
	@WebMethod String[] login(int id, String password);
	@WebMethod String[] getSchedule(int id);
	@WebMethod String getTeacher(int crn);
	@WebMethod String[] getStudentAttendance(int id);
	@WebMethod String[] getClassAttendance(int crn);
	@WebMethod String[] getCourseLocation(int crn);
}
