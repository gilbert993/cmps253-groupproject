package webservices;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(endpointInterface = "webservices.Database", portName = "DatabasePort", serviceName = "Database")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class DatabaseImp implements Database {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String MYSQL_DB_HOST = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
	static final String MYSQL_DB_PORT = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
	static final String DB_URL = "jdbc:mysql://" + MYSQL_DB_HOST + ":" + MYSQL_DB_PORT + "/253attendance";

	// Database credentials
	static final String USER = "adminaNWvx2J"; // TO BE REPLACED
	static final String PASS = "SNFuMd3N9ttN"; // TO BE REPLACED

	public String addStudent(int id, String email, String name, int c1, int c2, int c3, int c4, int c5, String password) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "INSERT INTO Student VALUES ("+ id + "," + "'"+email +"'" +","+ "'"+name+"'," +c1+"," +c2+"," +c3+"," +c4+"," +c5+",'" + password+"'"  +")";
			stmt.executeUpdate(sql);
			return "Inserted records into the table....";

		} catch (Exception e) {
			return e.getMessage();
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				return e.getMessage();
			}
		}
	}
	

	public String getStudent(String email) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT id,email,name FROM Student WHERE email = "+ "'" +email+"'" ;
			ResultSet rs = stmt.executeQuery(sql);
			String result="";
			while(rs.next())
			{
				int id = rs.getInt(1);
				String emailDB = rs.getString(2);
				String name = rs.getString(3);
				result+= id+ " " +emailDB +" "+name+ "\n";
			}
			return result;

		} catch (Exception e) {
			return e.getMessage();
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				return e.getMessage();
			}
		}
	}

	@Override
	public String[] login(int id, String password) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			//			 insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT password FROM Student WHERE id=" + id ;
			ResultSet rs = stmt.executeQuery(sql);
			String p="";
			while(rs.next()) p = rs.getString(1);
			if(p.equals(password)) {
				return login(id);
			}

			String[] errorMsg = {"Wrong password"};
			return errorMsg;

		} catch (Exception e) {
			String[] s = {e.getMessage()};
			return s;
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				String[] s = {e.getMessage()};
				return s;
			}
		}
	}

	private String[] login(int id) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String	sql = "SELECT name,email,course1,course2,course3,course4,course5 FROM Student WHERE id=" + id;
			ResultSet rs = stmt.executeQuery(sql);
			String[] str = new String[7];
			while(rs.next()) {
				for(int i = 0; i < 7; i++)
					str[i]=rs.getString(i+1);
			}
			return str;
		} catch (Exception e) {
			String[] s = {e.getMessage()};
			return s;
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				String[] s = {e.getMessage()};
				return s;
			}
		}
	}

	@Override
	public String[] getSchedule(int crn) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT m_time,t_time,w_time,r_time,f_time FROM Schedule WHERE CRN=" + crn ;
			ResultSet rs = stmt.executeQuery(sql);
			String[] sch = new String[5];
			while(rs.next()) {
				for(int i = 0; i < 5; i++)
					sch[i]=""+rs.getString(i+1);
			}
			return sch;
		} catch (Exception e) {
			String[] s = {e.getMessage()};
			return s;
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				String[] s = {e.getMessage()};
				return s;
			}
		}
	}

	@Override
	public String getTeacher(int crn) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT Teacher.name FROM Teacher INNER JOIN Course ON (Teacher.id=Course.teacher_id AND Course.CRN="+crn+")";
			ResultSet rs = stmt.executeQuery(sql);
			System.out.println("hey there");
			String result="";
			while(rs.next())
			{
				result= rs.getString(1);
			}
			return result;

		} catch (Exception e) {
			return e.getMessage();
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				return e.getMessage();
			}
		}
	}

	@Override
	public String[] getStudentAttendance(int id) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT CRN,Date,attended FROM Attendance WHERE student_id=" + id+" ORDER BY Date";
			ResultSet rs = stmt.executeQuery(sql);
			ArrayList<String> str = new ArrayList<String>();
			while(rs.next()) {
				str.add(rs.getString(1)+", " + rs.getString(2) + ", " + rs.getString(3));
			}
			return str.toArray((new String[str.size()]));

		} catch (Exception e) {
			String[] s = {e.getMessage()};
			return s;
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				String[] s = {e.getMessage()};
				return s;
			}
		}
	}

	@Override
	public String[] getClassAttendance(int crn) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String sql = "SELECT student_id,Date,attended FROM Attendance WHERE CRN=" + crn+" ORDER BY Date";
			ResultSet rs = stmt.executeQuery(sql);
			ArrayList<String> str = new ArrayList<String>();
			while(rs.next()) {
				str.add(rs.getString(1)+", " + rs.getString(2) + ", " + rs.getString(3));
			}
			return str.toArray((new String[str.size()]));

		} catch (Exception e) {
			String[] s = {e.getMessage()};
			return s;
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				String[] s = {e.getMessage()};
				return s;
			}
		}
	}

	@Override
	public String[] getCourseLocation(int crn) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER); // Register JDBC driver
			conn = DriverManager.getConnection(DB_URL, USER, PASS); // Open a connection
			stmt = conn.createStatement(); // Execute a query
			// insert a field to the array Transaction(int:key, int, int, NVARCHAR)
			String	sql = "SELECT longitude,latitude,height FROM Course WHERE CRN=" + crn;
			ResultSet rs = stmt.executeQuery(sql);
			String[] str = new String[3];
			while(rs.next()) {
				for(int i = 0; i < 3; i++)
					str[i]=""+rs.getInt(i+1);
			}
			return str;
		} catch (Exception e) {
			String[] s = {e.getMessage()};
			return s;
		} finally {
			try {
				if (stmt != null) conn.close();
			} catch (SQLException e) {
				String[] s = {e.getMessage()};
				return s;
			}
		}
	}


}
